/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******	ToolboxE.c ********************************************************\

Project:	Ursula (RISC OS for Risc PC II)
Component:	Windows configuration plug-in
This file:	Toolbox event code

History:
Date		Who	Change
----------------------------------------------------------------------------
09/12/1997	BJGA	Split from Main
			Added these headers
10/12/1997	BJGA	Made handler routines static
11/05/1998	BJGA	Added NumberRange_ValueChanged handler

\**************************************************************************/

/* Toolbox */
#include "event.h"
#include "toolbox.h"
#include "gadgets.h"
/* Common */
#include "cmos.h"
#include "misc.h"
/* local headers */
#include "Main.h"
#include "Settings.h"
#include "ToolboxE.h"  /* includes prototypes for this file */

static int toolboxe_actionbuttonselected (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle);
static int toolboxe_optionbuttonstatechanged (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle);
static int toolboxe_numberrangevaluechanged (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle);

int toolboxe_events [4] =     { ActionButton_Selected,
				OptionButton_StateChanged,
				NumberRange_ValueChanged,
				0 };

void toolboxe_register (void)
{
  throw (event_register_toolbox_handler (-1, ActionButton_Selected, toolboxe_actionbuttonselected, NULL));
  throw (event_register_toolbox_handler (-1, OptionButton_StateChanged, toolboxe_optionbuttonstatechanged, NULL));
  throw (event_register_toolbox_handler (-1, NumberRange_ValueChanged, toolboxe_numberrangevaluechanged, NULL));
}

/******	toolboxe_actionbuttonselected() ***********************************\

Purpose:	Handles ActionButton_Selected event

\**************************************************************************/

static int toolboxe_actionbuttonselected (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle)
{
  int gadget;
  switch (id_block->self_component) {
    case mainwindow_dragall:
      for (gadget = mainwindow_dragmin; gadget <= mainwindow_dragmax; gadget++) throw (optionbutton_set_state (0, mainwindow_id, gadget, 1));
      break;
    case mainwindow_dragnone:
      for (gadget = mainwindow_dragmin; gadget <= mainwindow_dragmax; gadget++) throw (optionbutton_set_state (0, mainwindow_id, gadget, 0));
      break;
    case mainwindow_default:
      settings_read (cmos_default);
      break;
    case mainwindow_cancel:
      if (!(event->hdr.flags & 1)) quit = TRUE;
      else settings_read (cmos_read);
      break;
    case mainwindow_set:
      if (settings_write ()) if (!(event->hdr.flags & 1)) quit = TRUE;
      break;
  }
  return 1;
}

/******	toolboxe_optionbuttonstatechanged() *******************************\

Purpose:	Handles OptionButton_StateChanged event

\**************************************************************************/

static int toolboxe_optionbuttonstatechanged (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle)
{
  if (id_block->self_component == mainwindow_submenuauto)
    misc_shadecomponents (!((OptionButtonStateChangedEvent *) event)->new_state,
      mainwindow_id, mainwindow_submenuauto_shademin, mainwindow_submenuauto_shademax);
  if (id_block->self_component == mainwindow_iconbarfwd)
    misc_shadecomponents (!((OptionButtonStateChangedEvent *) event)->new_state,
      mainwindow_id, mainwindow_iconbarfwd_shademin, mainwindow_iconbarfwd_shademax);
  return 1;
}

/******	toolboxe_numberrangevaluechanged() ********************************\

Purpose:	Handles NumberRange_ValueChanged event

\**************************************************************************/

static int toolboxe_numberrangevaluechanged (int event_code, ToolboxEvent *event, IdBlock *id_block, void *handle)
{
  ComponentId this_c, that_c;
  int offset = mainwindow_speed2 - mainwindow_speed; /* == mainwindow_accel2 - mainwindow_accel */
  int new_value = ((NumberRangeValueChangedEvent *) event) -> new_value;
  int other_value;

  this_c = id_block->self_component;
  that_c = (mainwindow_speed == this_c) ? mainwindow_accel : mainwindow_speed;
  throw (numberrange_get_value (0, mainwindow_id, that_c, &other_value));

  settings_update_display (mainwindow_id, this_c + offset, new_value);

  if ((0 == new_value) && (0 == other_value))
  {
    /* Don't allow both to be set to zero at the same time! */
    throw (numberrange_set_value (0, mainwindow_id, that_c, 1));
    settings_update_display (mainwindow_id, that_c + offset, 1);
  }

  return 1;
}
